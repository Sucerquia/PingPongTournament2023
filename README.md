**<h1> Doubles and Singles Ping Pong Tournament </h1>**
**<h2> HITS, 2023 </h2>**
Organized by Daniel Sucerquia (daniel.sucerquia@h-its.org)

**CONTENT**

- [Description](#description)
- [Groups](#groups)
  - [Groups Singles](#groups-singles)
  - [Group Doubles](#group-doubles)
- [Qualifiers](#qualifiers)
- [Suggested Schedule](#suggested-schedule)
- [Points System](#points-system)
- [Tournament Rules](#tournament-rules)

# Description

Both tournaments have a "[groups phase](#groups)" where all the payers of a group defy
all the other players of the same group. After a match, each player receives points
according to [the points system](#points-system). In the singles tournament,
the first two players of each group pass to [the qualifiers](#qualifiers). In
the doubles tournament, the first two teams play the final.

Please, follow the normal rules and the [tournament rules](#torunament-rules).

# Groups

| **Note** |
| ---         |
| The information on the next tables means: $`\begin{array}{c:c} {\color{red} points} &  {\color{blue} \Delta score/set} \\ \end{array} `$ |

## Groups Singles


```math
\begin{array}{|l|c|c|c|c|}
\hline
{\bf{Group A}} &  1 &  2 &  3 &  4\\ \hline
1 - Marcus &
\begin{array}{c:c} {\color{red} \space \space  9} & {\color{blue}               +19.0} \end{array} &
\begin{array}{c:c} {\color{red} \space \space  3} & {\color{blue} \space \space  +7.0} \end{array} &
\begin{array}{c:c} {\color{red} \space \space  3} & {\color{blue} \space \space  +6.0} \end{array} &
\begin{array}{c:c} {\color{red} \space \space  3} & {\color{blue} \space \space  +6.0} \end{array}\\ \hline

2 - Jannik &
\begin{array}{c:c} {\color{red} \space \space  0} & {\color{blue} \space \space  -7.0} \end{array} &
\begin{array}{c:c} {\color{red} \space \space  0} & {\color{blue}               -21.0} \end{array} &
\begin{array}{c:c} {\color{red} \space \space  0} & {\color{blue} \space \space  -6.0} \end{array} &
\begin{array}{c:c} {\color{red} \space \space  0} & {\color{blue} \space \space  -8.0} \end{array}\\ \hline

3 - Saber &
\begin{array}{c:c} {\color{red} \space \space  0} & {\color{blue} \space \space  -6.0} \end{array} &
\begin{array}{c:c} {\color{red} \space \space  3} & {\color{blue} \space \space  +6.0} \end{array} &
\begin{array}{c:c} {\color{red} \space \space  3} & {\color{blue} \space \space  -6.5} \end{array} &
\begin{array}{c:c} {\color{red} \space \space  0} & {\color{blue} \space \space  -6.5} \end{array}\\ \hline

4 - Marc-Oliver &
\begin{array}{c:c} {\color{red} \space \space  0} & {\color{blue} \space \space  -6.0} \end{array} &
\begin{array}{c:c} {\color{red} \space \space  3} & {\color{blue} \space \space  +8.0} \end{array} &
\begin{array}{c:c} {\color{red} \space \space  3} & {\color{blue} \space \space  +6.5} \end{array} &
\begin{array}{c:c} {\color{red} \space \space  6} & {\color{blue} \space \space  +8.5} \end{array}\\ \hline

\end{array}
```

```math
\begin{array}{|l|c|c|c|c|}
\hline
{\bf{Group B}} &  1 &  2 &  3 &  4\\ \hline
1 - Leif &
\begin{array}{c:c} {\color{red} \space \space  9} & {\color{blue} +15.5} \end{array} &
\begin{array}{c:c} {\color{red} \space \space  3} & {\color{blue} \space \space  +7.0} \end{array} &
\begin{array}{c:c} {\color{red} \space \space  3} & {\color{blue} \space \space  +2.5} \end{array} &
\begin{array}{c:c} {\color{red} \space \space  3} & {\color{blue} \space \space  +6.0} \end{array}\\ \hline

2 - Anna &
\begin{array}{c:c} {\color{red} \space \space 0} & {\color{blue} \space \space -7.0} \end{array} &
\begin{array}{c:c} {\color{red} \space \space 0} & {\color{blue} -19.5} \end{array} &
\begin{array}{c:c} {\color{red} \space \space 0} & {\color{blue} \space \space -7.5} \end{array} &
\begin{array}{c:c} {\color{red} \space \space 0} & {\color{blue} \space \space -5.0} \end{array}\\ \hline

3 - Quentin &
\begin{array}{c:c} {\color{red} \space \space 0} & {\color{blue} \space \space -2.5} \end{array} &
\begin{array}{c:c} {\color{red} \space \space 3} & {\color{blue} \space \space +7.5} \end{array} &
\begin{array}{c:c} {\color{red} \space \space 5} & {\color{blue} \space \space +4.0} \end{array} &
\begin{array}{c:c} {\color{red} \space \space 2} & {\color{blue} \space \space -1.0} \end{array}\\ \hline

4 - Daniel &
\begin{array}{c:c} {\color{red} \space \space 0} & {\color{blue} \space \space -6.0} \end{array} &
\begin{array}{c:c} {\color{red} \space \space 3} & {\color{blue} \space \space +5.0} \end{array} &
\begin{array}{c:c} {\color{red} \space \space 1} & {\color{blue} \space \space +1.0} \end{array} &
\begin{array}{c:c} {\color{red} \space \space 4} & {\color{blue} \space \space +0.0} \end{array}\\ \hline
\end{array}
```

```math
\begin{array}{|l|c|c|c|}
\hline
{\bf{Group C}} &  1 &  2 &  3\\ \hline
1 - Kai &
\begin{array}{c:c} {\color{red} \space \space 3} & {\color{blue} \space \space -4.0} \end{array} &
\begin{array}{c:c} {\color{red} \space \space 3} & {\color{blue} \space \space +4.5} \end{array} &
\begin{array}{c:c} {\color{red} \space \space 0} & {\color{blue} \space \space -8.5} \end{array}\\ \hline

2 - Johanna &
\begin{array}{c:c} {\color{red} \space \space 0} & {\color{blue} \space \space -4.5} \end{array} &
\begin{array}{c:c} {\color{red} \space \space 0} & {\color{blue}              -11.5} \end{array} &
\begin{array}{c:c} {\color{red} \space \space 0} & {\color{blue} \space \space -7.0} \end{array}\\ \hline

3 - Anthony &
\begin{array}{c:c} {\color{red} \space \space 3} & {\color{blue} \space \space +8.5} \end{array} &
\begin{array}{c:c} {\color{red} \space \space 3} & {\color{blue} \space \space +7.0} \end{array} &
\begin{array}{c:c} {\color{red} \space \space 6} & {\color{blue}              +15.5} \end{array}\\ \hline
\end{array}
```

```math
\begin{array}{|l|c|c|c|}
\hline
{\bf{Group D}} &  1 &  2 &  3\\ \hline
1 - Valentin &
\begin{array}{c:c} {\color{red} \space \space 4} & {\color{blue} \space \space  +5.3} \end{array} &
\begin{array}{c:c} {\color{red} \space \space 1} & {\color{blue} \space \space  -1.7} \end{array} &
\begin{array}{c:c} {\color{red} \space \space 3} & {\color{blue} \space \space  +7.0} \end{array}\\ \hline

2 - Eric &
\begin{array}{c:c} {\color{red} \space \space  2} & {\color{blue} \space \space  +1.7} \end{array} &
\begin{array}{c:c} {\color{red} \space \space  5} & {\color{blue} \space \space  +7.2} \end{array} &
\begin{array}{c:c} {\color{red} \space \space  3} & {\color{blue} \space \space  +5.5} \end{array}\\ \hline

3 - Kiril &
\begin{array}{c:c} {\color{red} \space \space 0} & {\color{blue} \space \space   -7.0} \end{array} &
\begin{array}{c:c} {\color{red} \space \space 0} & {\color{blue} \space \space  -5.5} \end{array} &
\begin{array}{c:c} {\color{red} \space \space 0} & {\color{blue}                -12.5} \end{array}\\ \hline

\end{array}
```
## Group Doubles


```math
\begin{array}{|l|c|c|c|c|c|}
\hline
Group D &  1 &  2 &  3 &  4 &  5\\ \hline
1 - Anthony-Quentin &
\begin{array}{c:c} {\color{red} \space \space  9} & {\color{blue} \space \space  +9.5} \end{array} &
\begin{array}{c:c} {\color{red} \space \space  2} & {\color{blue} \space \space  +0.3} \end{array} &
\begin{array}{c:c} {\color{red} \space \space  1} & {\color{blue} \space \space  -2.3} \end{array} &
\begin{array}{c:c} {\color{red} \space \space  3} & {\color{blue} \space \space  +4.0} \end{array} &
\begin{array}{c:c} {\color{red} \space \space  3} & {\color{blue} \space \space  +7.5} \end{array} \\ \hline

2 - Eric-Leif &
\begin{array}{c:c} {\color{red} \space \space  1} & {\color{blue} \space \space  -0.3} \end{array} &
\begin{array}{c:c} {\color{red} \space \space  8} & {\color{blue} \space \space  +2.9} \end{array} &
\begin{array}{c:c} {\color{red} \space \space  1} & {\color{blue} \space \space  -4.3} \end{array} &
\begin{array}{c:c} {\color{red} \space \space  3} & {\color{blue} \space \space  +5.5} \end{array} &
\begin{array}{c:c} {\color{red} \space \space  3} & {\color{blue} \space \space  +2.0} \end{array} \\ \hline

3 - Marcus-Valentin &
\begin{array}{c:c} {\color{red} \space \space  2} & {\color{blue} \space \space  +2.3} \end{array} &
\begin{array}{c:c} {\color{red} \space \space  2} & {\color{blue} \space \space  +4.3} \end{array} &
\begin{array}{c:c} {\color{red}               10} & {\color{blue}               +20.1} \end{array} &
\begin{array}{c:c} {\color{red} \space \space  3} & {\color{blue} \space \space  +8.0} \end{array} &
\begin{array}{c:c} {\color{red} \space \space  3} & {\color{blue} \space \space  +5.5} \end{array}\\ \hline

4 - Jannik-Saber &
\begin{array}{c:c} {\color{red} \space \space  0} & {\color{blue} \space \space  -4.0} \end{array} &
\begin{array}{c:c} {\color{red} \space \space  0} & {\color{blue} \space \space  -5.5} \end{array} &
\begin{array}{c:c} {\color{red} \space \space  0} & {\color{blue} \space \space  -8.0} \end{array} &
\begin{array}{c:c} {\color{red} \space \space  2} & {\color{blue}               -15.0} \end{array} &
\begin{array}{c:c} {\color{red} \space \space  2} & {\color{blue} \space \space  +2.5} \end{array}\\ \hline

5 - Johanna-Daniel &
\begin{array}{c:c} {\color{red} \space \space  0} & {\color{blue} \space \space  -7.5} \end{array} &
\begin{array}{c:c} {\color{red} \space \space  0} & {\color{blue} \space \space  -2.0} \end{array} &
\begin{array}{c:c} {\color{red} \space \space  0} & {\color{blue} \space \space  -5.5} \end{array} &
\begin{array}{c:c} {\color{red} \space \space  1} & {\color{blue} \space \space  -2.5} \end{array} &
\begin{array}{c:c} {\color{red} \space \space  1} & {\color{blue}               -17.5} \end{array}\\ \hline

\end{array}
```

# Qualifiers

## Singles

<img src="/qualifiers.png"  width="700">

## Doubles
<img src="/final-doubles.png"  width="700">


# Suggested Schedule

Except for the finals, each player must fix the time that fits better for each match.
However, I suggest the next schedule:

```math
\begin{array}{lc}
\text{Singles groupA}                & \text{July 04, 1-2pm}  \\ \hline
\text{Doubles 1, 2, 3}               & \text{July 05, 1-2pm}  \\ \hline
\text{Singles groupB}                & \text{July 06, 1-2pm}  \\ \hline
\text{Doubles 4, 5, 1}               & \text{July 07, 1-2pm}  \\ \hline
\text{Singles group C and D}              & \text{July 10, 1-2pm}  \\ \hline
\text{Doubles 2, 3, 4, 5}            & \text{July 11, 1-2pm}  \\ \hline
\text{Qualifiers}   & \text{July 12, 1-2pm}  \\ \hline
\text{\bf{Finals}}                   & \text{\bf{July 14, 5-6pm}}
\end{array}
```

| **Note** |
| ---         |
| Anyways, confirm with your opponents if they can be there at the suggested time.|

# Points System

The points from each game are given as follows: 

	 0 - lose in the second set 

	 1 - lose in the third set

	 2 - win in the third set

	 3 - win in the second set

The difference in the score is also considered in case of a tie.

# Tournament Rules

All games consist of max three sets played up to 11 points or
until the difference is grater than 2.

**Recommended info**
- Doubles serving: https://www.youtube.com/watch?v=ZWgX_k4Z1NQ&ab_channel=PingSkills
- Service rules: https://www.youtube.com/watch?v=s9ELscafqVs&ab_channel=PingSkills

